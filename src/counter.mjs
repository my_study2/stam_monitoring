class Counter {
    constructor(initialValue = 0) {
        this.value = initialValue;
    }

    increase() {
        this.value++;
    }

    zero() {
        this.value = 0;
    }

    read() {
        return this.value;
    }
}

export default Counter;