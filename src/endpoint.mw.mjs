/** @type {import{'express'}.RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    console.log(`[ENDPOINT] ${req.method} '${req.originalUrl}'`);
    if (req.method === 'GET' && req.originalUrl === '/counter-read') {
        console.log(`[COUNTER] read 0`); 
    } else if (req.method === 'GET' && req.originalUrl === '/counter-increase') {
        console.log(`[COUNTER] increase 1 (after the increase)`); 
    } else if (req.method === 'GET' && req.originalUrl === '/counter-zero') {
        console.log('[COUNTER] zeroed 0'); 
    }
    next();
};

export default endpoint_mw;